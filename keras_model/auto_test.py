import sys
import numpy as np
from itertools import izip
import seq2seq
from keras.layers.core import TimeDistributedDense, Activation, Dropout
from seq2seq.models import AttentionSeq2seq
sys.path.append("/home/jjery2243542/TS/summarization/")
from word2vec.sim import sim
from seq_creater.get_seq import Get_seq
batch_size = 20
model = AttentionSeq2seq(input_length=20, input_dim=256, hidden_dim=512, output_length=10, output_dim=512, depth=(2,1))
model.add(Activation("relu"))
#model.add(Activation("softplus"))
#model.add(Dropout(0.05))
model.add(TimeDistributedDense(output_dim=256, init="glorot_uniform"))
#model.compile(loss='cosine_proximity', optimizer="rmsprop")
model.compile(loss='mse', optimizer="rmsprop")
model.load_weights("./weight/2-1_model/mse_model/19_dep_2-1_weight.m5")
getter = sim()
for x,_,x_text_seq, y_text_seq in Get_seq(batch_size, "/tmp2/chinese_news/all_data/test_set"):
    y_seq_batch = model.predict_on_batch(x)[0]
    for y_seq, x_text, y_text in izip(y_seq_batch, x_text_seq, y_text_seq):
        print "-----paragraph---"
        print x_text.strip() 
        print "-----title------"
        print y_text.strip()
        print "-----result-----"
        for vec in y_seq:
            topn = getter.most_sim(vec)
            for word, sim in topn:
                print word, sim,
            print 

