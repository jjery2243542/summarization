import numpy as np
import theano
import theano.tensor as T
from itertools import izip
import pickle
#from theano.compile.nanguardmode import NanGuardMode
#from draw_tool.draw import plot
#from seq_creater.get_seq import Get_seq

n_in = 13
n_k = 128
sigma = lambda x: 1 / (1 + T.exp(-x))
dtype = theano.config.floatX
num_epochs=13
"""
def softmax(x):
    return T.exp(x) / T.sum(T.exp(x))
"""
def encode_step(e_t, h_tm1, c_tm1):
    IN_t = T.concatenate([e_t, h_tm1, c_tm1])
    P_t = theano.dot(IN_t, W_E) + b_E
    i_t = sigma(P_t[0:1 * n_k])
    f_t = sigma(P_t[1 * n_k:2 * n_k])
    o_t = sigma(P_t[2 * n_k:3 * n_k])
    l_t = T.tanh(P_t[3 * n_k:4 * n_k])
    c_t = f_t * c_tm1 + i_t * l_t
    h_t = o_t * T.tanh(c_t)
    return [h_t, c_t]

def decode_layer(h):
    return T.nnet.relu(theano.dot(h, W_he) + b_he)

def decode_step(e_tm1, h_tm1, c_tm1):
    IN_t = T.concatenate([e_tm1, h_tm1, c_tm1])
    P_t = theano.dot(IN_t, W_D) + b_D
    i_t = sigma(P_t[0:1 * n_k])
    f_t = sigma(P_t[1 * n_k:2 * n_k])
    o_t = sigma(P_t[2 * n_k:3 * n_k])
    l_t = T.tanh(P_t[3 * n_k:4 * n_k])
    c_t = f_t * c_tm1 + i_t * l_t
    h_t = o_t * T.tanh(c_t)
    #don't need to activate
    e_t = decode_layer(h_t)
    return [e_t, h_t, c_t]

def Update(params, gradients):
    return [(p, p - lr * T.clip(g, -1e-5, 1e-5)) for p, g in izip(params, gradients)] + [(lr, np.float32(0.99999) * lr)]
 
def write_params(params, dir_path="./params/"):
    for p in params:
        np.savetxt(dir_path + p.name, p.get_value())

def load_params(params, dir_path="./params/"):
    for p in params:
        p.set_value(np.cast[dtype](np.loadtxt(dir_path + p.name)))
    
#init_params
print "init params"
#input to embedding
W_xe = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = (n_in, n_k))),name="W_xe")  
b_xe = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_k)), name="b_xe")
#encode
W_E = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = (3*n_k, 4*n_k))),name="W_E")  
b_E = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = 4 * n_k)), name="b_E")
#decode
W_D = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = (3*n_k, 4*n_k))),name="W_D")  
b_D = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = 4 * n_k)),name="b_D")
#hidden to embedding
W_he = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = (n_k, n_k))),name="W_he")  
b_he = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_k)), name="b_he")
#embedding to output
W_ey = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = (n_k, n_in))),name="W_ey")  
b_ey = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_in)), name="b_ey")
#
#WD = theano.shared(np.cast[dtype](0.1 * np.identity(4 * n_k)), name="WD")
"""
W_a1 = theano.shared(np.cast[dtype](0.1 * np.identity(n_k)),name="W_a1")
W_a2 = theano.shared(np.cast[dtype](0.1 * np.identity(n_k)),name="W_a2")
b_a = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_k)),name="b_a")
U = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_k)),name="U")
W_he = theano.shared(np.cast[dtype](0.1 * np.identity(n_k)),name="W_he")
b_he = theano.shared(np.cast[dtype](np.random.uniform(-0.1,.1,size = n_k)),name="b_he")
"""
#learning rate
lr = theano.shared(np.cast[dtype](.1))
#params = [W_xe, b_xe, WE, bE, WD, bD, W_a1, W_a2, b_a, U, W_he, b_he, W_ew, b_ew]
params = [W_xe, b_xe, W_E, b_E, W_D, b_D, W_he, b_he, W_ey, b_ey]
load_params(params)
#define func
print "define func"
x_seq = T.matrix(dtype=dtype)
#y_hat_seq = T.matrix(dtype=dtype)
#change dim from n_in to n_k
eE_seq = T.nnet.relu(theano.dot(x_seq, W_xe) + b_xe)
# encoder hidden sequence
[hE_seq,_],_ = theano.scan(fn=encode_step, sequences=[eE_seq], outputs_info=[T.zeros_like(eE_seq[0]), T.zeros_like(eE_seq[0])], truncate_gradient=-1)
#decoder hidden sequence
[eD_seq,_,_],_ = theano.scan(fn=decode_step, outputs_info=[decode_layer(hE_seq[-1]), hE_seq[-1], T.zeros_like(eE_seq[0])], n_steps = x_seq.shape[0], truncate_gradient=-1)
y_seq = theano.dot(eD_seq, W_ey) + b_ey
#cost = T.mean((y_seq - x_seq) ** 2)# + T.sum([t ** 2 for t in params])
#gradients = T.grad(cost, params)
test_func = theano.function(inputs=[x_seq], outputs=[hE_seq[-1]])#, mode=NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True))
#init vector creater
samples = pickle.load(open("./seg_samples.pkl"))
vectors = []
for sample in samples:
    vector = train_func(np.array(sample, dtype=np.float32))
    vectors.append(vector)
pickle.dump(vectors, open("encode_vec.pkl", "w"))
