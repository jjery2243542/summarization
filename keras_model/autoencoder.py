import sys
import seq2seq
from keras.layers.core import TimeDistributedDense, Activation, Dropout
from keras.models import Sequential
from seq2seq.models import AttentionSeq2seq
sys.path.append("/home/jjery2243542/TS/summarization/")
from seq_creater.get_seq import Get_seq
batch_size = 20
model = AttentionSeq2seq(input_length=40, input_dim=256, hidden_dim=512, output_length=12, output_dim=512, depth=(2,1), dropout=0.05)
model.add(Activation("relu"))
model.add(Dropout(0.05))
model.add(TimeDistributedDense(output_dim=256))
model.compile(loss='mse', optimizer="rmsprop")
model.load_weights("weight/2-1_model/7_seq_40-12_dep_2-1_weight.m5")
print "-----2-1 model with relu-----"
print "input length=40, output length=12"
print "------training-------"
for epoch in range(7, 20):
    print "epoch #%d" % (epoch)
    for idx, (x, y,_,_) in enumerate(Get_seq(batch_size)):
        loss = model.train_on_batch(x, y)
        print "batch #%d, loss=%f" % (idx, loss[0])
        if idx % 30000 == 0:
            print "--------save weight-------"
            #model.save_weights("./weight/2-1_model/" + str(epoch)+ "_seq_40-12_dep_2-1_weight.m5", overwrite=True) 
    print "------validation-------"
    loss_sum = 0
    for idx, (x, y,_,_) in enumerate(Get_seq(batch_size, "/tmp2/chinese_news/all_data/test_set")):
        if idx == 500:
            break
        loss = model.evaluate(x, y, batch_size, verbose=0)
        loss_sum = loss_sum + loss
    print "mean_loss=%f" %(loss_sum / 500) 
