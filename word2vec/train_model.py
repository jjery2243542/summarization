#coding=utf-8
from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
import os
import logging

class MySentences(object):
    def __init__(self, dirname="/tmp2/chinese_news/all_data"):
        self.dirname = dirname

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            if fname[-4:] == ".seg":
                for line in open(os.path.join(self.dirname, fname)):
                    yield line.split()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
sentences = MySentences()
model = Word2Vec(sentences, size=1024, window=5, min_count=5, workers=7)
model.save("word2vec.mdl")
