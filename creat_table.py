import pickle
import sys
import string

path = sys.argv[1]
cnt_table = {}
doc_list_file = open(path + "/bbc.docs", "r")
for line in doc_list_file:
    line = line.strip().replace(".", "/") + ".txt"
    f = open(path + "/" + line, "r")
    for term in f.read().strip().split():
        term = term.translate(None, string.punctuation).lower()
        if term.isalpha():
            if term in cnt_table:
                cnt_table[term] += 1
            else: 
                cnt_table[term] = 1
del_set = set()
for key in cnt_table:
    if cnt_table[key] <= 1: 
        del_set.add(key)
        print key,
for del_word in del_set:
    del cnt_table[del_word]
lookup_table = {}
words = []
id = 0
for key in cnt_table:
    lookup_table[key] = id
    words.append(key)
    id = id + 1
lookup_table["<unk>"] = id
#lookup_table["<eos>"] = id + 1
words.append("<unk>")
#words.append("<eos>")
#lookup_table["<s>"] = id + 2
for key in lookup_table:
    print key, lookup_table[key]
print
print "eliminate %d words" % (len(del_set))
print "remaining words:%d" % (len(lookup_table))
output = open("./table.pkl", "ab+")
pickle.dump(lookup_table, output)
output.close()
output = open("./word.pkl", "ab+")
pickle.dump(words, output)
output.close()
