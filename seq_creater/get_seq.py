#coding=utf-8
from gensim.models import Word2Vec
import gensim.models
import os
import numpy as np
from itertools import izip_longest

class Get_seq(object):
    def __init__(self, batch_size, dirname="/tmp2/chinese_news/all_data/train_set"):
        self.dirname = dirname
        self.batch_size = batch_size
        self.model = Word2Vec.load("/tmp2/word2vec_mdl/model256/word2vec.mdl")
        self.batch_x = []
        self.batch_y = []
        self.x_text = []
        self.y_text = []

    def __iter__(self):
        # to extend the seq
        period_vec = self.model["。"]
        for fname in os.listdir(self.dirname):
            if fname[-4:] == ".seg":
                with open(os.path.join(self.dirname, fname), "r") as infile:
                    for y, x in izip_longest(infile, infile):
                        title_words = y.strip().split()
                        if len(title_words) > 7 and len(title_words) < 13:
                            y_vec = [self.model[word] for word in title_words if word in self.model]
                            x_vec = [self.model[word] for word in x.strip().split()[:40] if word in self.model]
                            if len(title_words)-len(y_vec) < 3 and len(x_vec) > 36:
                                self.x_text.append(x.strip())
                                self.y_text.append(y.strip())
                                #extend
                                for l in range(12 - len(y_vec)):
                                    y_vec.append(period_vec)
                                for l in range(40 - len(x_vec)):
                                    x_vec.append(period_vec)
                                if len(self.batch_x) < self.batch_size:
                                    self.batch_x.append(x_vec)
                                    self.batch_y.append(y_vec)
                                else:
                                    yield np.array(self.batch_x).astype(dtype="float32"), np.array(self.batch_y).astype(dtype="float32"), self.x_text, self.y_text
                                    #empty the batch list
                                    del self.batch_x[:]
                                    del self.batch_y[:]
                                    del self.x_text[:]
                                    del self.y_text[:]
