import string
import pickle
doc_path = "../bbc/bbc.docs"
doc_list_file = open(doc_path, "r")
doc_dir = "../bbc"
#f= open("./table.pkl", "r")
#table = pickle.load(f)
#print len(table)
for line in doc_list_file:
    #text
    f = open(doc_dir + "/" + line.strip().replace(".", "/") + ".txt", "r")
    title_word_seq = [x.translate(None, string.punctuation).lower() for x in f.readline().strip().split() if x.translate(None, string.punctuation).lower().isalpha()]
    title_id_seq = [table[x] if x in table else table["<unk>"] for x in title_word_seq]
    word_seq = [x.translate(None, string.punctuation).lower() for x in f.read().strip().split() if x.translate(None, string.punctuation).lower().isalpha()]
    id_seq = [table[x] if x in table else table["<unk>"] for x in word_seq]
    f.close()
    tra = open(doc_dir + "/" + line.strip().replace(".", "/") + ".tra", "w")
    for id in id_seq:
        tra.write(str(id) + " ")
    tra.close()
    lab = open(doc_dir + "/" + line.strip().replace(".", "/") + ".lab", "w")
    for id in title_id_seq:
        lab.write(str(id) + " ")
    lab.close()
