from seq2seq.layers.encoders import LSTMEncoder
from keras.layers.core import Dropout, TimeDistributedDense, Activation
from keras.layers.recurrent import LSTM
from keras.models import Sequential

model = Sequential()
model.add(LSTM(512, input_dim=256, return_sequences=True))
model.add(Dropout(0.25))
model.add(LSTMEncoder(input_dim=512, output_dim=512, state_input=False, return_sequences=True))
model.add(Dropout(0.25))
model.add(LSTMEncoder(input_dim=512, output_dim=512, state_input=False, return_sequences=True))
model.add(Dropout(0.25))
model.add(TimeDistributedDense(output_dim=512))
model.add(Activation("softplus"))
model.add(Dropout(0.25))
model.add(TimeDistributedDense(output_dim=256))
model.compile(loss='mse', optimizer="rmsprop")
for layer in model.layers:
    print layer 
    print layer.get_config()
    for w in layer.get_weights():
        print w.shape
