import numpy as np
import theano
import theano.tensor as T
from id_to_vec import id_to_vec 
from itertools import izip

n_in = 20045
n_k = 500
#n_in = 20
#n_k = 5
# squashing of the gates should result in values between 0 and 1
# therefore we use the logistic function
sigma = lambda x: 1 / (1 + T.exp(-x))
dtype = theano.config.floatX

def softmax(x):
    return T.exp(x) / T.sum(T.exp(x))

# for the other activation function we use the tanh

# sequences: e_t
# prior results: h_tm1, c_tm1
def encode_step(e_t, h_tm1, c_tm1):
    IN_t = T.concatenate([h_tm1, c_tm1, e_t])
    P_t = theano.dot(IN_t, WE) + bE
    i_t = sigma(P_t[0:1 * n_k])
    f_t = sigma(P_t[1 * n_k:2 * n_k])
    o_t = sigma(P_t[2 * n_k:3 * n_k])
    l_t = T.tanh(P_t[3 * n_k:4 * n_k])
    c_t = f_t * c_tm1 + i_t * l_t
    h_t = o_t * c_t
    return [h_t, c_t]

def decode_step(e_tm1, h_tm1, c_tm1, hE_seq):
    #T.nnet.softmax will change dim
    a_t = softmax(theano.dot(sigma(theano.dot(h_tm1, W_a1).dimshuffle('x', 0) + theano.dot(hE_seq, W_a2) + b_a.dimshuffle('x', 0)), U.T)).T
    m_t = theano.dot(a_t, hE_seq)
    IN_t = T.concatenate([h_tm1, c_tm1, e_tm1, m_t])
    P_t = theano.dot(IN_t, WD) + bD
    i_t = sigma(P_t[0:1 * n_k])
    f_t = sigma(P_t[1 * n_k:2 * n_k])
    o_t = sigma(P_t[2 * n_k:3 * n_k])
    l_t = T.tanh(P_t[3 * n_k:4 * n_k])
    c_t = f_t * c_tm1 + i_t * l_t
    h_t = o_t * c_t
    e_t = sigma(theano.dot(h_t, W_he) + b_he)
    return [e_t, h_t, c_t]

def sample_weights(sizeX, sizeY):
    values = np.ndarray([sizeX, sizeY], dtype=dtype)
    for dx in range(sizeX):
        vals = np.random.uniform(low=-1., high=1.,  size=(sizeY,))
        vals_norm = np.sqrt((vals**2).sum())
        vals = vals / vals_norm
        values[dx,:] = vals
    _,svs,_ = np.linalg.svd(values)
    #svs[0] is the largest singular value                      
    values = values / svs[0]
    return values

def Update(params, gradients):
    return [(p, p - lr * T.clip(g, -0.05, 0.05)) for p, g in izip(params, gradients)]
 
def write_params(params, dir_path="./params/"):
    for p in params:
        np.savetxt(dir_path + p.name, p.get_value())

def load_params(params, dir_path="./params/"):
    for p in params:
        p.set_value(np.cast[dtype](np.loadtxt(dir_path + p.name)))
    
# initialize weights
# i_t and o_t should be "open" or "closed"
# f_t should be "open" (don't forget at the beginning of training)
# we try to archive this by appropriate initialization of the corresponding biases

#init_params
print "init params"
W_xe = theano.shared(sample_weights(n_in, n_k), name="W_xe")
b_e = theano.shared(np.cast[dtype](np.random.uniform(-0.1, 0.1, size = n_k)),name="b_e")
#encode
WE = theano.shared(sample_weights(3 * n_k, 4 * n_k),name="WE")  
bE = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = 4 * n_k)), name="bE")
#decode
WD = theano.shared(sample_weights(4 * n_k, 4 * n_k),name="WD")
bD = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = 4 * n_k)),name="bD")
W_a1 = theano.shared(sample_weights(n_k, n_k),name="W_a1")
W_a2 = theano.shared(sample_weights(n_k, n_k),name="W_a2")
b_a = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = n_k)),name="b_a")
U = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = n_k)),name="U")
W_he = theano.shared(sample_weights(n_k, n_k),name="W_he")
b_he = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = n_k)),name="b_he")
W_ew = theano.shared(sample_weights(n_k, n_in),name="W_ew")
b_w = theano.shared(np.cast[dtype](np.random.uniform(-0.5,.5,size = n_in)),name="b_w")
#learning rate
lr = theano.shared(np.cast[dtype](.1))
params = [W_xe, b_e, WE, bE, WD, bD, W_a1, W_a2, b_a, U, W_he, b_he, W_ew, b_w]
#define func
print "define func"
x_seq = T.matrix(dtype=dtype)
y_hat_seq = T.matrix(dtype=dtype)

# transform to word embedding
eE_seq = sigma(theano.dot(x_seq, W_xe) + b_e.dimshuffle('x', 0))
#encoder first step
c0 = theano.shared(np.zeros(n_k, dtype=dtype))
h0 = T.tanh(c0)
# encoder hidden sequence
[hE_seq,_],_ = theano.scan(fn=encode_step, sequences=[eE_seq], outputs_info=[h0, c0], truncate_gradient=-1)
#decoder first step
c0 = hE_seq[-1]
h0 = T.tanh(c0)
e0 = sigma(theano.dot(h0, W_he) + b_he)
#decoder hidden sequence
[eD_seq,_,_],_ = theano.scan(fn=decode_step, outputs_info=[e0, h0, c0], non_sequences=[hE_seq], n_steps = 20, truncate_gradient=-1)
w_seq = T.nnet.softmax(sigma(theano.dot(eD_seq, W_ew) + b_w))
w_seq_resized = w_seq[:y_hat_seq.shape[0]]
cost = -T.sum(y_hat_seq * T.log(w_seq_resized) + (1 - y_hat_seq) * T.log(1 - w_seq_resized))# + T.sum([t ** 2 for t in params])
gradients = T.grad(cost, params)
train_func = theano.function(inputs=[x_seq, y_hat_seq], outputs=cost, updates=Update(params, gradients))
#init vector creater
creater = id_to_vec()
for epoch in range(50):
    print "epoch #%d" % (epoch)
    for doc in range(2175):
        print "doc #%d" % (doc)
        x, y = creater.get_vec_seq()
        print train_func(x[:30], y)
write_params(params)
