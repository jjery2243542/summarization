#encoding=utf-8
import jieba
import sys
jieba.set_dictionary("./dict.txt.big")
f = open(sys.argv[1], "r")
sent = f.read()
words = jieba.lcut(sent, cut_all=False)
for word in words:
    print word.encode("utf8"), " ",
print ""
