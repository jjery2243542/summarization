import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

def plot(fname, y):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(y)
    fig.savefig(fname.strip())
