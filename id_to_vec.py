import numpy as np
import pickle
class id_to_vec:
    def __init__(self, doc_path="../bbc/bbc.docs.test", table_path="./table.pkl", doc_dir="../bbc", words_path="./word.pkl"):
        self.doc_list_file = open(doc_path, "r")
        f = open(table_path, "rb")
        self.table = pickle.load(f)
        self.doc_dir = doc_dir
        f.close()
        f = open(words_path, "rb")
        self.words = pickle.load(f)
        f.close()

    def get_vec_seq(self):
        def id_to_vec(id):
            vec = np.zeros(len(self.table))
            vec[id] = 1
            return vec
        line = self.doc_list_file.readline()
        #end of file
        if line == "":
            self.doc_list_file.seek(0)
            line = self.doc_list_file.readline()
        #text
        f = open(self.doc_dir + "/" + line.strip().replace(".", "/") + ".tra", "r")
        vec_seq = [id_to_vec(int(x)) for x in f.read().strip().split()]
        print np.argmax(vec_seq, axis=1)
        f.close()
        #title
        f = open(self.doc_dir + "/" + line.strip().replace(".", "/") + ".lab", "r")
        target_vec_seq = [id_to_vec(int(x)) for x in f.read().strip().split()]
        print np.argmax(target_vec_seq, axis=1)
        f.close()
        return (np.array(vec_seq).astype(dtype="float32"), np.array(target_vec_seq).astype(dtype="float32"))
    def get_words(self, vec):
        result = np.argmax(vec, axis=1)
        return [self.words[x] for x in result]
